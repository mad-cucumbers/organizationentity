﻿using AutoMapper;
using Contracts.Enums;
using OrganizationEntity.Core;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.DataAccess.Service
{
    public class GenericService<TDto,TEntity> : IGenericService<TDto, TEntity> where TDto : BaseDto where TEntity : BaseEntity
    {
        protected readonly IGenericRepository<TDto, TEntity> _repository;
        protected readonly IMapper _mapper;

        public GenericService(IMapper mapper, IGenericRepository<TDto, TEntity> repository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<OrganizationEntityActionResult> AddAsync(TDto model)
        {
            var mappedEntity = _mapper.Map<TEntity>(model);

            //var entity = await _repository.GetEntityAsync(e => e.Id == model.Id);

            //if (entity.AspNetException != null) return entity;
            //else if (entity.Data == null)
            //    return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.NotExistPosition },
            //        $"Произошла ошибка при добавлении сущности || Не найдена модель || Модель: < {typeof(Position)} > || ID: < {model.Id} >");


            return await _repository.AddAsync(model);
        }

        public async Task<OrganizationEntityActionResult> DeleteAsync(int id)
        {
            var entity = await _repository.GetEntityAsync(e => e.Id == id);
            if (entity.AspNetException != null)
                return OrganizationEntityActionResult.Fail(entity.Errors, entity.AspNetException);
            if (entity.Data == null)
                return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.NotFound },
                    $"Ошибка при удалении модели || Модель не найдена || Модель: < {typeof(TDto)} > || ID: < {id} >");
            return await _repository.DeleteAsync(id);
        }

        public bool ExistEntityByName(string name, int? Tid = null)
        {
            throw new NotImplementedException();
        }

        public async Task<OrganizationEntityActionResult<IEnumerable<TDto>>> GetEntitiesAsync(Expression<Func<TDto, bool>> whereExpression = null)
        {
            var entity = await _repository.GetEntitiesAsync(whereExpression);
            if (entity.AspNetException != null)
                return OrganizationEntityActionResult<IEnumerable<TDto>>
                    .Fail(null, entity.Errors, entity.AspNetException);

            if (!entity.Data.Any())
                return OrganizationEntityActionResult<IEnumerable<TDto>>
                    .Fail(null, new[] { TypeOfErrors.NoContent }, $"Не найдено ниодной модели || Модель: < {typeof(Employee)} >");

            var response = _mapper.Map<IEnumerable<TDto>>(entity.Data);
            return OrganizationEntityActionResult<IEnumerable<TDto>>.IsSuccess(response);
        }

        public async Task<OrganizationEntityActionResult<TDto>> GetEntityAsync(Expression<Func<TDto, bool>> whereExpression)
        {
            var entity = await _repository.GetEntityAsync(whereExpression);
            if (entity.AspNetException != null)
                return OrganizationEntityActionResult<TDto>.Fail(null, entity.Errors, entity.AspNetException);

            if (entity.Data == null)
                return OrganizationEntityActionResult<TDto>
                    .Fail(null, new[] { TypeOfErrors.NoContent },
                    $"Не найдено ниодной модели || Модель: < {typeof(TDto)} > || Входной параметр:  < dsa >");

            var response = _mapper.Map<TDto>(entity.Data);
            return OrganizationEntityActionResult<TDto>.IsSuccess(response);
        }

        public async Task<OrganizationEntityActionResult> UpdateAsync(TDto model)
        {
            //var entity = await _repository.GetEntityAsync(e => e.Id == model.Id);
            //if (entity.AspNetException != null) return OrganizationEntityActionResult
            //        .Fail(entity.Errors, entity.AspNetException);
            //if (entity.Data == null)
            //    return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.NoContent },
            //        $"Ошибка при изменении модели || Модель не найдена || Модель < {typeof(Position)} > || ID < {model.Id} >");
            return await _repository.UpdateAsync(model);
        }
    }
}
