﻿using AutoMapper;
using Contracts.Enums;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.Constants;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.DataAccess.Service
{
    public class BuildingService : GenericService<BuildingDto, Building>, IBuildingService
    {
        public BuildingService(IMapper mapper, IBuildingRepository repository) : base(mapper, repository)
        {
        }

    }
}
