﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;
using OrganizationEntity.DataAccess.Data;

namespace OrganizationEntity.DataAccess.Repositories
{
    public class EfSubDivisionRepository :EfGenericRepository<SubdivisionDto, Subdivision>, ISubdivisionRepository
    {
        public EfSubDivisionRepository(IMapper mapper, AppDbContext context) : base(mapper, context)
        {
        }
    }
}
