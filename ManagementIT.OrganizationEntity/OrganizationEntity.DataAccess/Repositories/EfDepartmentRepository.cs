﻿using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Models;
using AutoMapper;

namespace OrganizationEntity.DataAccess.Repositories
{
    public class EfDepartmentRepository : EfGenericRepository<DepartmentDto, Department>, IDepartmentRepository
    {
        public EfDepartmentRepository(IMapper mapper, AppDbContext context) : base(mapper, context)
        {
            Includes = new Expression<Func<Department, object>>[]
            {
                dependency => dependency.Subdivision
            };
        }
    }
}
