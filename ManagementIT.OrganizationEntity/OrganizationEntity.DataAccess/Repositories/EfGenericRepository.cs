﻿using AutoMapper;
using Contracts.Enums;
using Microsoft.EntityFrameworkCore;
using OrganizationEntity.Core;
using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.ResponseModels;
using OrganizationEntity.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.DataAccess.Repositories
{
    public class EfGenericRepository<TDto, TEntity> : IGenericRepository<TDto, TEntity> 
        where TEntity : BaseEntity where TDto : BaseDto
    {
        protected readonly IMapper _mapper;
        protected readonly AppDbContext _context;
        protected Expression<Func<TEntity, object>>[] Includes;

        public EfGenericRepository(IMapper mapper, AppDbContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<OrganizationEntityActionResult> AddAsync(TDto model)
        {
            try
            {
                var entity = _mapper.Map<TEntity>(model);
                _context.Set<TEntity>().Add(entity);
                await _context.SaveChangesAsync();
                return OrganizationEntityActionResult.IsSuccess();
            }
            catch (Exception e)
            {
                return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.AddingEntityError },
                    $"Ошибка при добавлении модели || Модель: < {typeof(TEntity)} > || Описание перехваченного исключения: < {e.InnerException} >");
            }
        }

        public async Task<OrganizationEntityActionResult<IEnumerable<TDto>>> GetEntitiesAsync(Expression<Func<TDto, bool>> whereExpression = null)
        {
            try
            {
                Expression<Func<TEntity, bool>> tentityExpression = null;
                if (whereExpression != null) tentityExpression = _mapper.Map<Expression<Func<TEntity, bool>>>(whereExpression);

                IQueryable<TEntity> set = _context.Set<TEntity>();
                if (Includes != null) set = Includes.Aggregate(set, (current, IncludeProp) => current.Include(IncludeProp));
                var result = tentityExpression == null ? await set.ToListAsync() : await set.Where(tentityExpression).ToListAsync();

                if (result.Count() == 0) return OrganizationEntityActionResult<IEnumerable<TDto>>.Fail(null, new[] { TypeOfErrors.NoContent },
                     $"{(tentityExpression == null ? "Получение полного списка моделей" : "Получение списка по заданному условию")} || Модель: < {typeof(TEntity)} > || {(tentityExpression == null ? "Данные не найдены" : "Данных удовлетворяющих условию нет" )}");
                var response = _mapper.Map<IEnumerable<TDto>>(result);
                return OrganizationEntityActionResult<IEnumerable<TDto>>.IsSuccess(response);
            }
            catch (Exception e)
            {
                return OrganizationEntityActionResult<IEnumerable<TDto>>
                    .Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"{(whereExpression == null ? "Ошибка: Получение полного списка моделей" : "Ошибка: Получение списка по заданному условию")} || Модель: < {typeof(TEntity)} > || Описание исключения: < {e.InnerException} >");
            }
        }

        public async Task<OrganizationEntityActionResult<TDto>> GetEntityAsync(Expression<Func<TDto, bool>> whereExpression)
        {
            try
            {
                var tentityExpression = _mapper.Map<Expression<Func<TEntity, bool>>>(whereExpression);
                IQueryable<TEntity> set = _context.Set<TEntity>();
                if (Includes != null) set = Includes.Aggregate(set, (current, IncludeProp) => current.Include(IncludeProp));
                var result = await set.FirstOrDefaultAsync(tentityExpression);

                if (result == null) return OrganizationEntityActionResult<TDto>.Fail(null, new[] { TypeOfErrors.NoContent },
                     $"Ошибка: получение модели по входному параметру || Модель: < {typeof(TEntity)} > || Данных удовлетворяющих условию нет");
                var response = _mapper.Map<TDto>(result);
                return OrganizationEntityActionResult<TDto>.IsSuccess(response);
            }
            catch (Exception e)
            {
                return OrganizationEntityActionResult<TDto>
                    .Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка: получение модели по параметру || Модель: < {typeof(TEntity)} > || Описание исключения: < {e.InnerException} >");
            }
        }


        public async Task<OrganizationEntityActionResult> UpdateAsync(TDto model)
        {
            try
            {
                var entity = _mapper.Map<TEntity>(model);
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return OrganizationEntityActionResult.IsSuccess();
            }
            catch (Exception e)
            {
                return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка при изменении модели || Модель: < {typeof(TEntity)} >  || ID < {model.Id} > || Name < {model.Name} > || Описание: < {e.InnerException} >");
            }
        }

        public async Task<OrganizationEntityActionResult> DeleteAsync(int id)
        {
            try
            {
                IQueryable<TEntity> set = _context.Set<TEntity>();
                var entity = await set.FirstOrDefaultAsync(x => x.Id == id);
                if (entity == null) return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.NotFound },
                     $"Ошибка: запрос на удаление модели || Тип модели: < {typeof(TEntity)} > || ID: <{id}> || Модель не найдена");
                _context.Entry(entity).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
                return OrganizationEntityActionResult.IsSuccess();

            }
            catch (Exception e)
            {
                return OrganizationEntityActionResult.Fail(new[] { TypeOfErrors.DeletionEntityError },
                    $"Ошибка: запрос на удаление модели || Тип модели: < {typeof(TEntity)} > || ID: < {id} > || Описание исключения: < {e.InnerException} >");
            }
        }

        public bool ExistEntity(string name, int? Tid = null) => 
            !Tid.HasValue ? _context.Set<TEntity>().Any(x => x.Name == name) : _context.Set<TEntity>().Where(x => x.Id != Tid).Any(x => x.Name == name);
    }
}
