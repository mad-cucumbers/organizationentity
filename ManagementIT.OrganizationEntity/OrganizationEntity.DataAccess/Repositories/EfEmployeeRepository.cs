﻿using Contracts.Enums;
using Microsoft.EntityFrameworkCore;
using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Constants;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.ResponseModels;
using OrganizationEntity.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.DataAccess.Repositories
{
    public class EfEmployeeRepository : EfGenericRepository<EmployeeDto, Employee>, IEmployeeRepository
    {
        public EfEmployeeRepository(IMapper mapper, AppDbContext context) : base(mapper, context)
        {
            Includes = new Expression<Func<Employee, object>>[]
            {
                dependency => dependency.Department,
                dependency => dependency.Department.Subdivision,
                dependency => dependency.Position,
                dependency => dependency.Photo
            };
        }
        
    }
}
