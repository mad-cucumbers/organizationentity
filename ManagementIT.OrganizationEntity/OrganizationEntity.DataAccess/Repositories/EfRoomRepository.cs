﻿using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Models;
using AutoMapper;

namespace OrganizationEntity.DataAccess.Repositories
{
    public class EfRoomRepository : EfGenericRepository<RoomDto, Room>, IRoomRepository
    {
        public EfRoomRepository(IMapper mapper, AppDbContext context) : base(mapper, context)
        {
            Includes = new Expression<Func<Room, object>>[]
            {
                dependency => dependency.Department,
                dependency => dependency.Department.Subdivision,
                dependency => dependency.Building
            };
        }
    }
}
