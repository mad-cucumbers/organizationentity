using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using OrganizationEntity.DataAccess.Data;
using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.Abstractions.OrganizationEntityRepository;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.DataAccess.Service;
using OrganizationEntity.WebHost.AutoMapper;
using OrganizationEntity.Core.Domain;
using AutoMapper.Extensions.ExpressionMapping;
using OrganizationEntity.DataAccess.Repositories;

namespace OrganizationEntity.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;
        
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value);
            });

            var mapperConfig = new MapperConfiguration(x =>
            {
                x.AddProfile(new MappingProfile());
                x.AddExpressionMapping();
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            #region DI


            services.AddTransient<IBuildingRepository, EfBuildingRepository>();
            services.AddTransient<IDepartmentRepository, EfDepartamentRepository>();
            services.AddTransient<IEmployeeRepository, EfEmployeeRepository>();
            //services.AddTransient<IBuildingRepository, EfBuildingRepository>();
            services.AddTransient<IPositionRepository, EfPositionRepository>();
            services.AddTransient<IRoomRepository, EfRoomRepository>();
            services.AddTransient<ISubdivisionRepository, EfSubDivisionRepository>();


            services.AddTransient<IBuildingService, BuildingService>();
            services.AddTransient<IDepartmentService, DepartamentService>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            //services.AddTransient<IBuildingService, BuildingService>();
            services.AddTransient<IPositionService, PositionService>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<ISubdivisionService, SubdivisionService>();

            #endregion

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OrganizationEntity.WebHost", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(s =>
                {
                    s.SwaggerEndpoint("/swagger/v1/swagger.json", "OrganizationEntity v1");
                    s.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List);
                    s.RoutePrefix = string.Empty;
                });
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseRouting();
            app.UseHttpsRedirection();



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
