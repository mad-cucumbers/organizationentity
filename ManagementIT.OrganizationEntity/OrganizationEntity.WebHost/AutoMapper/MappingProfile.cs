﻿using AutoMapper;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.WebHost.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<Employee, EmployeeDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ForMember("Surname", dest => dest.MapFrom(src => src.Surname))
                .ForMember("Patronymic", dest => dest.MapFrom(src => src.Patronymic))
                .ForMember("Department", dest => dest.MapFrom(src => src.Department))
                .ForMember("Position", dest => dest.MapFrom(src => src.Position))
                .ForMember("WorkTelephone", dest => dest.MapFrom(src => src.WorkTelephone))
                .ForMember("MobileTelephone", dest => dest.MapFrom(src => src.MobileTelephone))
                .ForMember("Mail", dest => dest.MapFrom(src => src.Mail))
                .ForMember("UserName", dest => dest.MapFrom(src => src.UserName))
                .ForMember("Photo", dest => dest.MapFrom(src => src.Photo))
                .ReverseMap();

            CreateMap<Department, DepartmentDto>()
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Subdivision, dest => dest.MapFrom(src => src.Subdivision))
                .ReverseMap();

            CreateMap<Building, BuildingDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ForMember("Address", dest => dest.MapFrom(src => src.Address))
                .ForMember("Floor", dest => dest.MapFrom(src => src.Floor))
                .ReverseMap();

            CreateMap<EmployeePhoto, EmployeePhotoDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ForMember("Photo", dest => dest.MapFrom(src => src.Photo))
                .ReverseMap();

            CreateMap<Position, PositionDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ReverseMap();

            CreateMap<Room, RoomDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ForMember("Building", dest => dest.MapFrom(src => src.Building))
                .ForMember("Department", dest => dest.MapFrom(src => src.Department))
                .ForMember("Floor", dest => dest.MapFrom(src => src.Floor))
                .ForMember("RequiredCountSocket", dest => dest.MapFrom(src => src.RequiredCountSocket))
                .ForMember("CurrentCountSocket", dest => dest.MapFrom(src => src.CurrentCountSocket))
                .ReverseMap();

            CreateMap<Subdivision, SubdivisionDto>()
                .ForMember("Id", dest => dest.MapFrom(src => src.Id))
                .ForMember("Name", dest => dest.MapFrom(src => src.Name))
                .ReverseMap();

            CreateMap<Building, BuildingDto>()
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Address, dest => dest.MapFrom(src => src.Address))
                .ForMember(x => x.Floor, dest => dest.MapFrom(src => src.Floor))
                .ReverseMap();


            CreateMap<Subdivision, SubdivisionDto>()
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ReverseMap();

            CreateMap<Employee, EmployeeDto>()
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Department, dest => dest.MapFrom(src => src.Department))
                .ForMember(x => x.Mail, dest => dest.MapFrom(src => src.Mail))
                .ForMember(x => x.MobileTelephone, dest => dest.MapFrom(src => src.MobileTelephone))
                .ForMember(x => x.Patronymic, dest => dest.MapFrom(src => src.Patronymic))
                .ForMember(x => x.Photo, dest => dest.MapFrom(src => src.Photo))
                .ForMember(x => x.Position, dest => dest.MapFrom(src => src.Position))
                .ForMember(x => x.Surname, dest => dest.MapFrom(src => src.Surname))
                .ForMember(x => x.UserName, dest => dest.MapFrom(src => src.UserName))
                .ForMember(x => x.WorkTelephone, dest => dest.MapFrom(src => src.WorkTelephone))
                .ReverseMap();

            CreateMap<EmployeePhoto, EmployeePhotoDto>()
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Photo, dest => dest.MapFrom(src => src.Photo))
                .ReverseMap();

            CreateMap<Position, PositionDto>()
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name));

            CreateMap<Room, RoomDto>()
                .ForMember(x => x.Id, dest => dest.MapFrom(src => src.Id))
                .ForMember(x => x.Name, dest => dest.MapFrom(src => src.Name))
                .ForMember(x => x.Building, dest => dest.MapFrom(src => src.Building))
                .ForMember(x => x.RequiredCountSocket, dest => dest.MapFrom(src => src.RequiredCountSocket))
                .ForMember(x => x.CurrentCountSocket, dest => dest.MapFrom(src => src.CurrentCountSocket))
                .ForMember(x => x.Floor, dest => dest.MapFrom(src => src.Floor))
                .ForMember(x => x.Department, dest => dest.MapFrom(src => src.Department))
                .ReverseMap();
        }
    }
}