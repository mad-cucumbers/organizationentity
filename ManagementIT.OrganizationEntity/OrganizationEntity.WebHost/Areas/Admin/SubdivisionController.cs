﻿using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    public class SubdivisionController : GenericController<SubdivisionDto, Subdivision>
    {
        public SubdivisionController(ISubdivisionService service) : base(service)
        {
        }
    }
}
