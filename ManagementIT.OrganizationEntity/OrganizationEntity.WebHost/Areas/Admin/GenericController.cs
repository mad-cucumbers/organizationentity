﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;
using OrganizationEntity.Core.ResponseModels;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    [Route("[controller]")]
    public abstract class GenericController<TDto, TEntity> : ControllerBase
                where TDto : BaseDto
                where TEntity: BaseEntity
    {
        protected readonly IGenericService<TDto, TEntity> _service;
        protected GenericController(IGenericService<TDto, TEntity> service)
        {
            _service = service;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> Create(TDto dto)
        {
            if (!ModelState.IsValid) return NotFound();
            var result = await _service.AddAsync(dto);
            return Ok(result);
        }
        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _service.GetEntitiesAsync();
            return  Ok(result);
        }
        [Route("[action]")]
        [HttpPut]
        public async Task<ActionResult> Update(TDto model)
        {
            if (!ModelState.IsValid) return NotFound();
            var result = await _service.UpdateAsync(model);
            return Ok(result);

        }
        [Route("[action]")]
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _service.DeleteAsync(id);
            return  Ok(result);
        }
    }
}
