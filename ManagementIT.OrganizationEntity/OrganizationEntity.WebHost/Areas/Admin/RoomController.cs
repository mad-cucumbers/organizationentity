﻿using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;
using OrganizationEntity.DataAccess.Service;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    public class RoomController : GenericController<RoomDto, Room>
    {
        public RoomController(IRoomService service) : base(service)
        {
        }
    }
}
