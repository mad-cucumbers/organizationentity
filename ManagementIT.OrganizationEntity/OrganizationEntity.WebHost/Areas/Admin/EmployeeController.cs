﻿using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;
using OrganizationEntity.DataAccess.Service;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    public class EmployeeController : GenericController<EmployeeDto, Employee>
    {
        public EmployeeController(IEmployeeService service) : base(service)
        {
        }
    }
}
