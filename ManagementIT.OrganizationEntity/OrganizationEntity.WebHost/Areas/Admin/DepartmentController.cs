﻿using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    public class DepartmentController : GenericController<DepartmentDto, Department>
    {
        public DepartmentController(IDepartmentService service) : base(service)
        {
        }
    }
}
