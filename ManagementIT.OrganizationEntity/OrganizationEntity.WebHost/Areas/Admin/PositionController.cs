﻿using Microsoft.AspNetCore.Mvc;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.WebHost.Areas.Admin
{
    public class PositionController : GenericController<PositionDto, Position>
    {
        public PositionController(IPositionService service) : base(service)
        {
        }
    }
}
