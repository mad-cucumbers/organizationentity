﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Contracts.Constants;
using Contracts.Enums;
using Contracts.ResponseModels;
using GreenPipes;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using OrganizationEntity.Core.Abstractions.Service;
using OrganizationEntity.Core.Models.DepartmentModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;

namespace OrganizationEntity.WebHost.Areas.Admin.RabbitMQ.DepartmentConsumer
{
    public class CreateDepartmentConsumer : IConsumer<CreateDepartmentViewModel>
    {
        private readonly IDepartamentService _departamentService;

        public CreateDepartmentConsumer(IDepartamentService departamentService)
        {
            _departamentService = departamentService ?? throw new ArgumentNullException(nameof(departamentService));
        }

        public async Task Consume(ConsumeContext<CreateDepartmentViewModel> context)
        {
            var existName = _departamentService.ExistEntityByName(context.Message.Name);
            if (!existName)
            {
                var model = new DepartmentDTO(context.Message.Name, context.Message.SubdivisionId);
                var result = await _departamentService.AddAsync(model);

                var response = result.Success
                    ? new NotificationViewModel()
                    : new NotificationViewModel(result.Errors, e: result.AspNetException);

                await context.RespondAsync<NotificationViewModel>(response);
            }
            else
                await context.RespondAsync<NotificationViewModel>(
                    new NotificationViewModel(new[] { TypeOfErrors.ExistNameEntity }));
        }
    }

    public class CreateDepartmentConsumerDefinition : ConsumerDefinition<CreateDepartmentConsumer>
    {
        public CreateDepartmentConsumerDefinition()
        {
            EndpointName = ApiShowConstants.CreateDepartment;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<CreateDepartmentConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}