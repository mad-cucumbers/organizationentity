﻿namespace OrganizationEntity.Core.Models
{
    public class EmployeeDto : BaseDto
    {
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentDto Department { get; set; }
        public int PositionId { get; set; }
        public PositionDto Position { get; set; }
        public string WorkTelephone { get; set; }
        public string MobileTelephone { get; set; }
        public string Mail { get; set; }
        public string UserName { get; set; }
        public int PhotoId { get; set; }
        public EmployeePhotoDto Photo { get; set; }
    }
}