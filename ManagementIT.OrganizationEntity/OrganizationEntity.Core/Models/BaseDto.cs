﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizationEntity.Core.Models
{
    public class BaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
