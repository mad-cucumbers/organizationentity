﻿namespace OrganizationEntity.Core.Models
{
    public class EmployeePhotoDto : BaseDto
    {
        public byte[] Photo { get; set; }
    }
}