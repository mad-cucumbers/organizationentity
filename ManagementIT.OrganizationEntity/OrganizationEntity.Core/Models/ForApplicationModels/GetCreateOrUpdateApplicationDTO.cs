﻿using System.Collections.Generic;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.Core.Models.ForApplicationModels
{
    public class GetCreateOrUpdateApplicationDTO
    {
        public List<RoomDto> SelectRoom { get; set; }
        public List<DepartatontDTO> SelectDepartment { get; set; }
        public List<EmployeeDto> SelectEmployee { get; set; }

        public GetCreateOrUpdateApplicationDTO() { }

        public GetCreateOrUpdateApplicationDTO(List<RoomDto> sekectRoom, List<DepartatontDTO> selectDept, List<EmployeeDto> selectEmployee)
        {
            SelectDepartment = selectDept;
            SelectEmployee = selectEmployee;
            SelectRoom = sekectRoom;
        }
    }
}