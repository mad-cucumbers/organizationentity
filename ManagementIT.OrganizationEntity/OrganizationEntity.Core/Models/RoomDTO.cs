﻿namespace OrganizationEntity.Core.Models
{
    public class RoomDto : BaseDto
    {
        public BuildingDto Building { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentDto Department { get; set; }
        public int Floor { get; set; }
        public int RequiredCountSocket { get; set; }
        public int CurrentCountSocket { get; set; }
    }
}