﻿namespace OrganizationEntity.Core.Models
{
    public class DepartatontDTO : BaseDto
    {
        public  int SubdivisionId { get; set; }
        public SubdivisionDto Subdivision { get; set; }
    }
}