﻿namespace OrganizationEntity.Core.Models
{
    public class BuildingDto : BaseDto
    {
        public string Address { get; set; }
        public int Floor { get; set; }
    }
}