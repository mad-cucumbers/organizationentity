﻿namespace OrganizationEntity.Core.Models
{
    public class DepartmentDto : BaseDto
    {
        public  int SubdivisionId { get; set; }
        public SubdivisionDto Subdivision { get; set; }
    }
}