﻿using OrganizationEntity.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.Core.Abstractions.TEntityRepository
{
    public interface IGenericRepository<TDto,TEntity> where TEntity:BaseEntity where TDto : BaseDto
    {
        Task<OrganizationEntityActionResult> AddAsync(TDto model);
        Task<OrganizationEntityActionResult<IEnumerable<TDto>>> GetEntitiesAsync(Expression<Func<TDto, bool>> whereExpression = null);
        Task<OrganizationEntityActionResult<TDto>> GetEntityAsync(Expression<Func<TDto, bool>> whereExpression);
        Task<OrganizationEntityActionResult> UpdateAsync(TDto model);
        Task<OrganizationEntityActionResult> DeleteAsync(int id);

        bool ExistEntity(string name, int? Tid = null);
    }
}
