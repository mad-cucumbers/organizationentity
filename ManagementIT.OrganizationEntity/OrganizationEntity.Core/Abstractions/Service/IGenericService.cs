﻿using OrganizationEntity.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.Core.Abstractions.Service
{
    public interface IGenericService<TDto, TEntity> where TDto : BaseDto where TEntity : BaseEntity
    {
        Task<OrganizationEntityActionResult> AddAsync(TDto model);
        Task<OrganizationEntityActionResult<IEnumerable<TDto>>> GetEntitiesAsync(Expression<Func<TDto, bool>> whereExpression = null);
        Task<OrganizationEntityActionResult<TDto>> GetEntityAsync(Expression<Func<TDto, bool>> whereExpression);
        Task<OrganizationEntityActionResult> UpdateAsync(TDto model);
        Task<OrganizationEntityActionResult> DeleteAsync(int id);
        bool ExistEntityByName(string name, int? Tid = null);
    }
}
