﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.Core.Abstractions.Service
{
    public interface IEmployeeService : IGenericService<EmployeeDto, Employee>
    {
    }
}
