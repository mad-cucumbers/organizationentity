﻿using OrganizationEntity.Core.Models;
using OrganizationEntity.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Domain;

namespace OrganizationEntity.Core.Abstractions.Service
{
    public interface IDepartmentService : IGenericService<DepartmentDto, Department>
    {
    }
}
