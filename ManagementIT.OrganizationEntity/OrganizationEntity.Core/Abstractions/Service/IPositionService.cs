﻿using OrganizationEntity.Core.Models;
using OrganizationEntity.Core.ResponseModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using OrganizationEntity.Core.Domain;

namespace OrganizationEntity.Core.Abstractions.Service
{
    public interface IPositionService : IGenericService<PositionDto, Position>
    {
    }
}
