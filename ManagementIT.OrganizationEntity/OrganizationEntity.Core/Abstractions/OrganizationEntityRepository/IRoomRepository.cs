﻿using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrganizationEntity.Core.Models;

namespace OrganizationEntity.Core.Abstractions.OrganizationEntityRepository
{
    public interface IRoomRepository : IGenericRepository<RoomDto, Room>
    {
    }
}
