﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OrganizationEntity.Core.Abstractions.TEntityRepository;
using OrganizationEntity.Core.Domain;
using OrganizationEntity.Core.Models;
using OrganizationEntity.Core.ResponseModels;

namespace OrganizationEntity.Core.Abstractions.OrganizationEntityRepository
{
    public interface IEmployeeRepository : IGenericRepository<EmployeeDto, Employee>
    {
    }
}