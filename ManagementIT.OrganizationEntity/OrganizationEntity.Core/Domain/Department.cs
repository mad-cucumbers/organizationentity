﻿namespace OrganizationEntity.Core.Domain
{
    public class Department : BaseEntity
    {
        public int SubdivisionId { get; set; }
        public Subdivision Subdivision { get; set; }
    }
}