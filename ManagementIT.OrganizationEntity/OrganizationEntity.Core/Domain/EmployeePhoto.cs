﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OrganizationEntity.Core.Domain
{
    public class EmployeePhoto : BaseEntity
    {
        public byte[] Photo { get; set; }
    }
}