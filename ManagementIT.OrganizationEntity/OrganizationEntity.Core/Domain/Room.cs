﻿namespace OrganizationEntity.Core.Domain
{
    public class Room : BaseEntity
    {
        public Building Building { get; set; }
        public int DepartamentId { get; set; }
        public Department Department { get; set; }
        public int Floor { get; set; }
        public int RequiredCountSocket { get; set; }
        public int CurrentCountSocket { get; set; }
    }
}