﻿namespace Contracts.Constants
{
    public class ApiShowConstants
    {
        public const string Success = "Запрос обработан";
        public const string Failed = "Произошла ошибка";

        public static string CreatePriority = "CreatePriority";
        public static string AllPriority = "AllPriority";
        public static string ByIdPriority = "ByIdPriority";
        public static string UpdatePriority = "UpdatePriority";
        public static string DeletePriority = "DeletePriority";

        public static string AllType = "AllType";
        public static string ByIdType = "ByIdType";
        public static string CreateType = "CreateType";
        public static string UpdateType = "UpdateType";
        public static string DeleteType = "DeleteType";
        
        public static string AllState = "AllState";
        public static string ByIdState = "ByIdState";
        public static string CreateState = "CreateState";
        public static string UpdateState = "UpdateState";
        public static string DeleteState = "DeleteState";
        
        public static string AllApplication = "AllApplication";
        public static string ByIdApplication = "ByIdApplication";
        public static string CreateApplication = "CreateApplication";
        public static string UpdateApplication = "UpdateApplication";
        public static string DeleteApplication = "DeleteApplication";
        public static string UpdateStateApplication = "UpdateStateApplication";
        public static string OnDeleteApplication = "OnDeleteApplication";
        public static string ByDeptIdApplication = "ByDeptIdApplication";
        public static string ByOnDeleteApplication = "ByOnDeleteApplication";
        public static string DeleteRangeApplication = "DeleteRangeApplication";
        public static string GetCreateApplication = "GetCreateApplication";
        public static string GetUpdateApplication = "GetUpdateApplication";
        
        public static string GetAllAction = "GetAllAction";
        public static string GetByIdAction = "GetByIdAction";
        public static string GetByEnumTypeAction = "GetByEnumTypeAction";
        public static string DeleteAllAction = "DeleteAllAction";
        public static string DeleteByIdAction = "DeleteByIdAction";
        public static string DeleteSelectAction = "DeleteSelectAction";

        public static string GetAllBuilding = "GetAllBuilding";
        public static string GetByIdBuilding = "GetByIdBuilding";
        public static string CreateBuilding = "CreateBuilding";
        public static string UpdateBuilding = "UpdateBuilding";
        public static string DeleteBuilding = "DeleteBuilding";
        
        public static string GetAllDepartment = "GetAllDepartment";
        public static string GetByIdDepartment = "GetByIdDepartment";
        public static string CreateDepartment = "CreateDepartment";
        public static string UpdateDepartment = "UpdateDepartment";
        public static string GetCreateDepartment = "GetCreateDepartment";
        public static string GetUpdateDepartment = "GetUpdateDepartment";
        public static string DeleteDepartment = "DeleteDepartment";
        
        public static string GetDependencyForApplication = "GetDependencyForApplication";
        
        public static string GetAllEmployee = "GetAllEmployee";
        public static string GetByIdEmployee = "GetByIdEmployee";
        public static string CreateEmployee = "CreateEmployee";
        public static string UpdateEmployee = "UpdateEmployee";
        public static string UpdateEmployeePhoto = "UpdateEmployeePhoto";
        public static string GetCreateEmployee = "GetCreateEmployee";
        public static string GetUpdateEmployee = "GetUpdateEmployee";
        public static string DeleteEmployeePhoto = "DeleteEmployeePhoto";
        public static string DeleteEmployee = "DeleteEmployee";
        
        public static string GetAllPosition = "GetAllPosition";
        public static string GetByIdPosition = "GetByIdPosition";
        public static string CreatePosition = "CreatePosition";
        public static string UpdatePosition = "UpdatePosition";
        public static string DeletePosition = "DeletePosition";
        
        public static string GetAllRoom = "GetAllRoom";
        public static string GetByIdRoom = "GetByIdRoom";
        public static string CreateRoom = "CreateRoom";
        public static string UpdateRoom = "UpdateRoom";
        public static string DeleteRoom = "DeleteRoom";
        public static string GetCreateRoom = "GetCreateRoom";
        public static string GetUpdateRoom = "GetUpdateRoom";
        
        public static string GetAllSubdivision = "GetAllSubdivision";
        public static string GetByIdSubdivision = "GetByIdSubdivision";
        public static string CreateSubdivision = "CreateSubdivision";
        public static string UpdateSubdivision = "UpdateSubdivision";
        public static string DeleteSubdivision = "DeleteSubdivision";

        public static string CreateLog = "CreateLogMessage";
        public static string GetAllLog = "GetAllLogMessage";
        public static string GetLogById = "GetLogByIdMessage";
        public static string GetLogByAddress = "GetLogByIdMessage";
        public static string GetLogByDateOrTime = "GetLogByDateOrTime";
        public static string DeleteLogById = "DeleteLogById";
        public static string DeleteRangeLogs = "DeleteRangeLogs";
        public static string DeleteSelectedLogs = "DeleteSelectedLogs";
    }
}