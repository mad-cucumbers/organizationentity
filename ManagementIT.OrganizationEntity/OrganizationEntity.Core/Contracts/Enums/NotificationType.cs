﻿namespace Contracts.Enums
{
    public enum NotificationType
    {
        Success = 1,
        Info,
        Warn,
        Error
    }
}