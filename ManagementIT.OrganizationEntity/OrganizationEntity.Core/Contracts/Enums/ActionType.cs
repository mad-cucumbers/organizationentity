﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Enums
{
    public enum ActionType
    {
        Creation = 1,
        Change,
        StateChange,
        Deletion,
        ChangeType,
        ChangePriority,
        ChangeRoom,
        ChangeDepartment,
        ChangeEmployee,
        AddingArchive
    }
}
